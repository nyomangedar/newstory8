from django.test import TestCase
from django.test import TestCase, Client
from selenium import webdriver
from django.urls import resolve
from .views import index
from selenium import webdriver
import unittest
import time

# Create your tests here.

class FunctionalTest(TestCase):
    def setUP(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def checkPageTitle(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/')
        self.assertIn('Document', selenium.title)

        selenium = self.selenium
        selenium.get('http://localhost:8000/message')

        selenium.implicitly_wait(5)

        self.assertIn('test1', selenium.page_source)
        self.assertIn('Hello World', selenium.page_source)

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

class Unittest(TestCase):
    def test_urls_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_view_use_correct_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_inside_html(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Accordion", response_content)

    

